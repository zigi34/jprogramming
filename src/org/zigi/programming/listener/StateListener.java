package org.zigi.programming.listener;

public interface StateListener<T> {
	public void handleStateChanged(Object soource, T eventCode);
}
